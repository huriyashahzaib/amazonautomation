module.exports = {

    EC : protractor.ExpectedConditions,

    //Navigation
    navSignIn : element(by.id('nav-link-accountList')),
    
    userNameElem : element(by.id('ap_email')),
    passwordElem : element(by.id('ap_password')),
    signInButton : element(by.id('signInSubmit')),
    addToCart : element(by.id('add-to-cart-button')),
    noThanksBtn : element(by.id('siNoCoverage-announce')),
    proceedToCheckOutBtn : element(by.name('proceedToCheckout')),
    productTitle : element(by.id('productTitle')),
    price : element(by.id('priceblock_ourprice')),
    cartCount : element(by.id('nav-cart-count')),
    viewCartBtn : element(by.id('hlb-view-cart-announce')),
    proceedToCheckOutBtn : element(by.name('proceedToCheckout')),
    addressBtn : element(by.linkText("Ship to this address")),
    saveGiftOptionBtn: element(by.className("a-button-inner")),
    shipOptionSelectBtn : element(by.className('a-button-text')),
};