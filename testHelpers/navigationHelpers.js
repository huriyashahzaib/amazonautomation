﻿module.exports = {

    goToApplication: function () {
        browser.driver.manage().window().maximize();
        browser.ignoreSynchronization = true;
        browser.get('https://amazon.com');
    }, 

    goToLoginPage : function () {
        var navSignIn = element(by.id('nav-link-accountList'));
        navSignIn.click();
    },

    goToProductPage : function(product) {
        var navProductPage = element(by.id('nav-link-shopall'));
        navProductPage.click();
        element(by.linkText(product)).click();
    },

    goToBestSeller : function () { 
        this.secondLevelNavigation("Best Sellers");
    },

    secondLevelNavigation : function (link) { 
        element(by.linkText(link)).click();
    },

   getPageUrl : function () {
       return browser.getCurrentUrl().then(function (pageUrl){
           return pageUrl;
       });
   },
   
};