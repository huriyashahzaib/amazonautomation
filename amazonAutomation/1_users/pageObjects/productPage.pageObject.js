var testHelpers = require('../../../testHelpers/testHelpers.js');
var navHelper = testHelpers.navigationHelpers;
var inputHelper = testHelpers.inputHelpers;
var elemSelector = testHelpers.elementSelect;

module.exports = {

    goToProduct : function (itemNo) {
        element.all(by.className('zg_itemImmersion')).then(function (items){
            items[itemNo-1].click(); 
        })
    },
   
   getPageUrl : function () {
       return browser.getCurrentUrl().then(function (pageUrl){
           return pageUrl;
       });
   },
   
   getExpectedPrice : function (itemNumber) {
        return element.all(by.className('p13n-sc-price')).then(function(items){
            items[itemNumber-1].getText().then(function(productPrice){
                return productPrice;
            })
        });
   },

   getProductTitle : function () { 
        return elemSelector.productTitle.getText().then(function(title){
            return title;
        });
    },

    getProductPrice : function () {
        return elemSelector.price.getText().then(function(price){
            return price;
        });
    }

};