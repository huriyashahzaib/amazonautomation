var testHelpers = require('../../../testHelpers/testHelpers.js');
var navHelper = testHelpers.navigationHelpers;
var inputHelper = testHelpers.inputHelpers;
var elemSelector = testHelpers.elementSelect;

module.exports = {

   proceedToCheckOut : function () { 
       elemSelector.proceedToCheckOutBtn.click();
   },

   selectShippingAddress : function () {
       elemSelector.addressBtn.click();
   },

   selectGiftOption : function () {
       elemSelector.saveGiftOptionBtn.click();
   },

   selectShippingOption : function () {
       elemSelector.shipOptionSelectBtn.click();
   },

   verifyPageUrl : function (expectedUrl){
       navHelper.getPageUrl().then(function (url){
           expect(url).toEqual(expectedUrl);
       });
   }
};