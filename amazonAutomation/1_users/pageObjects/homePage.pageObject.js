﻿var testHelpers = require('../../../testHelpers/testHelpers.js');
var navHelper = testHelpers.navigationHelpers;
var inputHelper = testHelpers.inputHelpers;
var elemSelector = testHelpers.elementSelect;

module.exports = {

    loginToAmazon: function (userName, password) {
        navHelper.goToApplication();
        navHelper.goToLoginPage();
        inputHelper.inputText(elemSelector.userNameElem,userName);
        inputHelper.inputText(elemSelector.passwordElem,password);
        elemSelector.signInButton.click();
    },

    getCategoryList : function (product) {
        navHelper.goToProductPage(product);
        navHelper.goToBestSeller();
    }

};