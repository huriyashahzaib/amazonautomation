var testHelpers = require('../../../testHelpers/testHelpers.js');
var navHelper = testHelpers.navigationHelpers;
var inputHelper = testHelpers.inputHelpers;
var elemSelector = testHelpers.elementSelect;

module.exports = {

   addToCart : function () { 
        elemSelector.addToCart.click();
        browser.sleep(2000);
        elemSelector.noThanksBtn.isPresent().then (function(result){
            if (result){
                elemSelector.noThanksBtn.click();
                browser.sleep(1000);
            }
        })
   },

   getCartCount : function () {
       return elemSelector.cartCount.getText().then(function(count){
           return count;
       });
   },

   goToCart : function () {
       elemSelector.viewCartBtn.click();
   }

};