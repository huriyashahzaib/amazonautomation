﻿///<reference path = "typings/tsd.d.ts" />

var homePageObject = require('../pageObjects/homePage.pageObject.js');
var shoppingCartObject = require('../pageObjects/shoppingCart.pageObject.js');
var productPageObject = require('../pageObjects/productPage.pageObject.js');
var inputObject = require('../../../testHelpers/inputFile.js')

describe('product selection', function () {
    var EC = protractor.ExpectedConditions;

    var expectedProductName = [];
    var expectedProductPrice = [];

    beforeAll(function (){
        homePageObject.loginToAmazon(inputObject.userName,inputObject.password)
        inputObject.numberOfItems.forEach(function(itemNumber){
            homePageObject.getCategoryList(inputObject.product);
            browser.sleep(1000);
            productPageObject.getExpectedPrice(itemNumber).then(function(price){
                expectedProductPrice.push(price);
            })
            productPageObject.goToProduct(itemNumber);
            shoppingCartObject.addToCart();
         });
    });

    it ('verify that cart count is increased', function (){
        shoppingCartObject.getCartCount().then(function(count){
            expect(parseInt(count)).toEqual(numberOfItems.length);
        })
    });

    it ('verify that user is on cart view page',function (){
        productPageObject.getPageUrl().then(function(url){
            expect(url).toContain("view");
        })
    });

    it ('verify that user can go to cart page',function(){
        shoppingCartObject.goToCart();
        productPageObject.getPageUrl().then(function (url){
            expect(url).toContain("cart");
        })
    });

    it ('verify that all items have correct price',function () {
        index = numberOfItems;
        element.all(by.className('a-size-medium a-color-price sc-price sc-white-space-nowrap sc-product-price sc-price-sign a-text-bold')).then(function(products){
            products.forEach(function(product){
                product.getText().then(function (price){
                    expect(price).toEqual(expectedProductPrice[index]);
                    index = index - 1;
                });
            });
        });
    });

    it('verify that cart total is correct',function (){
        cartTotal = 0;
        expectedProductPrice.forEach(function(price){
            cartTotal = cartTotal + price;
        })
        element(by.className('a-size-medium a-color-price sc-price sc-white-space-nowrap  sc-price-sign')).getText().then(function(total){
            expect(total).toEqual(cartTotal);
        })
    });
});
