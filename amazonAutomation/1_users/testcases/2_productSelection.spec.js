﻿///<reference path = "typings/tsd.d.ts" />

var homePageObject = require('../pageObjects/homePage.pageObject.js');
var shoppingCartObject = require('../pageObjects/shoppingCart.pageObject.js');
var productPageObject = require('../pageObjects/productPage.pageObject.js');
var inputObject = require('../../../testHelpers/inputFile.js')
describe('product selection', function () {
    var EC = protractor.ExpectedConditions;
    var expectedProductName = "";
    var expectedProductPrice = "";

    beforeAll(function (){
        homePageObject.loginToAmazon(inputObject.userName,inputObject.password)
        inputObject.numberOfItems.forEach(function(itemNumber){
            homePageObject.getCategoryList(inputObject.product);
            browser.sleep(1000);
            element.all(by.className('p13n-sc-truncated-hyphen p13n-sc-truncated')).then(function(items){
                items[itemNumber-1].getAttribute("title").then(function(productName){
                    expectedProductName = productName;
                });
            });
            productPageObject.getExpectedPrice(itemNumber).then(function(productPrice){
                expectedProductPrice = productPrice;
            })
            productPageObject.goToProduct(itemNumber);
         });
    });

    it ('verify that user is on correct page', function (){
        productPageObject.getPageUrl().then(function(url){
            expect(url).toContain(expectedProductName);
        })
    });

    it('verify product selected is correct', function () {
        productPageObject.getProductTitle().then(function(productTitle){
            expect(productTitle).toContain(expectedProductName);
        });
    });

    it ('verify product price is correct', function () {
        productPageObject.getProductPrice().then(function(productPrice){
            expect(productPrice).toEqual(expectedProductPrice);
        });
    });
});
