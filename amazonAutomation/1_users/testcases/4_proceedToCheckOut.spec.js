﻿///<reference path = "typings/tsd.d.ts" />

var homePageObject = require('../pageObjects/homePage.pageObject.js');
var shoppingCartObject = require('../pageObjects/shoppingCart.pageObject.js');
var productPageObject = require('../pageObjects/productPage.pageObject.js');
var checkOutPageObject = require('../pageObjects/checkOut.pageObject.js');
var inputObject = require('../../../testHelpers/inputFile.js')

describe('product selection', function () {
    var EC = protractor.ExpectedConditions;

    var expectedProductName = "";
    var expectedProductPrice = "";

    beforeAll(function (){
        homePageObject.loginToAmazon(inputObject.userName,inputObject.password)
        inputObject.numberOfItems.forEach(function(itemNumber){
            homePageObject.getCategoryList(inputObject.product);
            browser.sleep(1000);
            productPageObject.goToProduct(itemNumber);
            shoppingCartObject.addToCart();
            checkOutPageObject.proceedToCheckOut();
         });
    });

    it ('verify that user is on address page', function (){
        checkOutPageObject.verifyPageUrl("addressselect");
    });

    it ('verify that user is on gift options page', function () {
        checkOutPageObject.selectShippingAddress();
        checkOutPageObject.verifyPageUrl("gift");
    });

    it('verify that user is on shipping options page',function () {
        checkOutPageObject.selectGiftOption();
        checkOutPageObject.verifyPageUrl("shipoptionselect");
    });

    it('verify that user is on payment page',function (){
        checkOutPageObject.selectShippingAddress();
        checkOutPageObject.verifyPageUrl("payselect");
    });

});
