﻿var HtmlReporter = require('protractor-jasmine2-html-reporter');

var reporter = new HtmlReporter({
    savePath: 'C:/Protractor/',
    takeScreenshotsOnlyOnFailures: true,
    screenshotsFolder: 'images'
});
exports.config = {
    //The address of a running selenium server.
    seleniumAddress: 'http://localhost:4444/wd/hub',

    //Capabilities to be passed to the webdriver instance.
    //will run two browsers simultaneously
    multiCapabilities: [{
            'browserName': 'firefox',
            maxInstances: 4
        }, 
        {
            'browserName': 'chrome',
            maxInstances: 4
        }],

    allScriptsTimeout: 110000,
    getPageTimeout: 100000,


    //Specify the name of the specs files.
    specs: ['amazonAutomation/1_users/testcases/*.spec.js'],

    //Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        onComplete: null,
        isVerbose: true,
        showColors: true,
        includeStackTrace: true,
        defaultTimeoutInterval: 400000
    },

    framework: 'jasmine2',

    // create xml reports using jasmine
    onPrepare: function () {
        var jasmineReporters = require('jasmine-reporters');

        // returning the promise makes protractor wait for the reporter config before executing tests
        return browser.getProcessedConfig().then(function (config) {
            // you could use other properties here if you want, such as platform and version
            var browserName = config.capabilities.browserName;

            var junitReporter = new jasmineReporters.JUnitXmlReporter({
                consolidateAll: false,
                savePath: 'testresults',
                // this will produce distinct xml files for each capability
                filePrefix: browserName + '-xmloutput',
                modifySuiteName: function (generatedSuiteName, suite) {
                    // this will produce distinct suite names for each capability,
                    // e.g. 'firefox.login tests' and 'chrome.login tests'
                    return browserName + '.' + generatedSuiteName;
                }
            });
            jasmine.getEnv().addReporter(reporter);
        });
    }
};