#Amazon Automation

Setup Guide
===========
Clone repository
```
git clone https://huriyashahzaib@bitbucket.org/huriyashahzaib/amazonautomation.git
```
Install protractor
```
npm install protractor -g
```
Install node dependencies for this project
```
cd /path/to/repository
npm install
```

How to run:
===========
Start webdriver manager in a command promt window
```
webdriver-manager start
```
Run config file from repository in another command prompt window
```
cd /path/to/repository
protractor config.js
```
